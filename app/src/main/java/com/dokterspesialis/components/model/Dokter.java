package com.dokterspesialis.components.model;

import android.graphics.drawable.Drawable;

public class Dokter {

    public String id_dokter;
    public String spesialis;
    public String nama;
    public String alamat;
    public String latitude;
    public String longitude;

}
