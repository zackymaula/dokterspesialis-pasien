package com.dokterspesialis.components.activity.button;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.dokterspesialis.components.R;

public class ButtonBasic extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button_basic);
    }
}
