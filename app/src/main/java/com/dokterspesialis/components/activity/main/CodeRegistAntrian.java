package com.dokterspesialis.components.activity.main;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dokterspesialis.components.R;
import com.dokterspesialis.components.activity.assets.CodeConfig;
import com.dokterspesialis.components.utils.Tools;

import java.util.Hashtable;
import java.util.Map;

public class CodeRegistAntrian extends AppCompatActivity {

    String intentIdDokter,
            intentSpesialis,
            intentNama,
            intentAlamat,
            intentLatitude,
            intentLongitude;

    ProgressDialog pDialog;

    EditText editNama, editAlamat;

    double pLong, pLat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_daftar_antrian);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Upload data ke server ...");
        pDialog.setCancelable(false);

        editNama = (EditText)findViewById(R.id.editTextNamaPasien);
        editAlamat = (EditText)findViewById(R.id.editTextAlamatPasien);

        checkPermissionAndSetLocation();
        getIntentString();
        clickRegist();
        initToolbar();
    }

    public void getIntentString(){
        Intent intent = getIntent();
        intentIdDokter = intent.getStringExtra("id_dokter");
        intentSpesialis = intent.getStringExtra("spesialis");
        intentNama = intent.getStringExtra("nama");
        intentAlamat = intent.getStringExtra("alamat");
        intentLatitude = intent.getStringExtra("latitude");
        intentLongitude = intent.getStringExtra("longitude");

        ((TextView)findViewById(R.id.textViewRegistNamaDokter)).setText(intentNama);
        ((TextView)findViewById(R.id.textViewRegistAlamatDokter)).setText(intentAlamat);
        ((TextView)findViewById(R.id.textViewRegistSpesialisDokter)).setText(intentSpesialis);
    }

    public void checkPermissionAndSetLocation(){
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationListener ll = new myLocationListener();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
        }
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, ll);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.grey_60), PorterDuff.Mode.SRC_ATOP);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
    }

    public void clickRegist(){
        findViewById(R.id.lyt_btn_registrasi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editNama.getText().toString().equals("") || editAlamat.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Mohon diisi semua", Toast.LENGTH_SHORT).show();
                } else {
                    prosesUploadAntrian();
                    //Toast.makeText(getApplicationContext(), "Terisi", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.lyt_btn_go_maps).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+Double.toString(pLat)+","+Double.toString(pLong)+"&daddr="+intentLatitude+","+intentLongitude));
                startActivity(intent);*/
                String URLDirection = "http://maps.google.com/maps?saddr="+Double.toString(pLat)+","+Double.toString(pLong)+"&daddr="+intentLatitude+","+intentLongitude;

                Intent i = new Intent(getBaseContext(), CodeMapDirection.class);
                i.putExtra("urlDirection", URLDirection);
                i.putExtra("nama", intentNama);
                i.putExtra("spesialis", intentSpesialis);
                i.putExtra("alamat", intentAlamat);
                startActivity(i);
            }
        });
    }

    private void showDialogRegist(String nomorAntrian) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_regist);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        ((TextView)dialog.findViewById(R.id.textViewDialogNomorAntrian)).setText(nomorAntrian);
        ((ImageButton) dialog.findViewById(R.id.btn_dialog_regist_save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Tersimpan", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    //menampilkan progress dialog
    void showProgressDialog(){
        if(!pDialog.isShowing())pDialog.show();
    }

    //menutup progress dialog
    void dismissProgressDialog(){
        if(pDialog.isShowing())pDialog.dismiss();
    }

    public void prosesUploadAntrian(){
        showProgressDialog();
        int socketTimeout = 10000;//10 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        StringRequest requestUpload = new StringRequest(Request.Method.POST, CodeConfig.URL_INSERT_PASIEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dismissProgressDialog();
                        //Toast.makeText(getBaseContext(), response,Toast.LENGTH_LONG).show();
                        showDialogRegist(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dismissProgressDialog();
                        Toast.makeText(getBaseContext(),"Gagal simpan data",Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new Hashtable<>();
                params.put("nama",editNama.getText().toString());
                params.put("alamat",editAlamat.getText().toString());
                params.put("id_dokter",intentIdDokter);
                return params;
            }
        };
        requestUpload.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(requestUpload);
    }

    class myLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            try {
                if(location != null) {
                    pLong = location.getLongitude();
                    pLat = location.getLatitude();
                }
            } catch (Exception e){

            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_setting, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.grey_60));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
