package com.dokterspesialis.components.activity.assets;

public class CodeConfig {

    static String ip_main = "http://172.20.10.4/dokter_spesialis/";

    public static final String URL_GET_DOKTER = ip_main+"mobile/list_dokter/";
    public static final String URL_INSERT_PASIEN = ip_main+"mobile/insert_antrian";
}
