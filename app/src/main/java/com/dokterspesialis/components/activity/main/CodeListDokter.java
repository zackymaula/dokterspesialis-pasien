package com.dokterspesialis.components.activity.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dokterspesialis.components.R;
import com.dokterspesialis.components.activity.assets.CodeConfig;
import com.dokterspesialis.components.adapter.AdapterListDokter;
import com.dokterspesialis.components.model.Dokter;
import com.dokterspesialis.components.utils.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CodeListDokter extends AppCompatActivity {

    private View parent_view;

    private RecyclerView recyclerView;
    private AdapterListDokter mAdapter;

    public static List<Dokter> items =  new ArrayList<>();

    String key_intent,
            str_spesialis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_list_dokter);
        parent_view = findViewById(android.R.id.content);

        items.clear();
        getStringIntent();
        getDokter();
        initToolbar();
        initComponent();
    }

    public void getStringIntent(){
        Intent intent = getIntent();
        key_intent = intent.getStringExtra("key");
        switch (key_intent){
            case "1":
                str_spesialis = "Gigi";
                break;
            case "2":
                str_spesialis = "Anak";
                break;
            case "3":
                str_spesialis = "Mata";
                break;
            case "4":
                str_spesialis = "Kulit";
                break;
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Dokter Spesialis "+str_spesialis);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this);
    }

    private void initComponent() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        //set data and list adapter
        mAdapter = new AdapterListDokter(this, items);
        recyclerView.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterListDokter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Dokter obj, int position) {
                //Toast.makeText(getBaseContext(), "Item " + obj.id_dokter + " clicked", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getBaseContext(), CodeRegistAntrian.class);
                i.putExtra("id_dokter", obj.id_dokter);
                i.putExtra("spesialis", obj.spesialis);
                i.putExtra("nama", obj.nama);
                i.putExtra("alamat", obj.alamat);
                i.putExtra("latitude", obj.latitude);
                i.putExtra("longitude", obj.longitude);
                startActivity(i);
            }
        });
    }

    public void getDokter(){
        int socketTimeout = 10000;//10 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                CodeConfig.URL_GET_DOKTER + key_intent,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray item = response.getJSONArray("result");

                            for (int i = 0; i < item.length(); i++) {
                                JSONObject person = item.getJSONObject(i);

                                Dokter obj = new Dokter();
                                obj.id_dokter = person.getString("id_dokter");
                                obj.spesialis = person.getString("spesialis");
                                obj.nama = person.getString("nama");
                                obj.alamat = person.getString("alamat");
                                obj.latitude = person.getString("latitude");
                                obj.longitude = person.getString("longitude");
                                items.add(obj);
                            }

                            initComponent();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getBaseContext(),"Dokter kosong",Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getBaseContext(),"Tidak ada koneksi internet",Toast.LENGTH_SHORT).show();
                    }
                }
        );
        jsonObjectRequest.setRetryPolicy(policy);
        Volley.newRequestQueue(CodeListDokter.this).add(jsonObjectRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_setting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
